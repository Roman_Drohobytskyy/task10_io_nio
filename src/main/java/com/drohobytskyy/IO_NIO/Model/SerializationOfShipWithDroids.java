package com.drohobytskyy.IO_NIO.Model;

import com.drohobytskyy.IO_NIO.View.ConsoleView;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

public class SerializationOfShipWithDroids {
    public ConsoleView view;
    Ship ship;
    Ship shipFromFile;

    public void initShip() {
        Droid droid1 = new Droid("r45", 12, 56, "stopImmediately");
        Droid droid2 = new Droid("superDroid", 10, 99, "stopStop");
        List<Droid> droids = new LinkedList<Droid>();
        droids.add(droid1);
        droids.add(droid2);
        ship = new Ship("Interstellar", ShipType.TRANSPORT, droids);
        view.logger.info(ship);
    }


    public void serializeAndDeserializeShipWithDroids() {
        initShip();
        try {
            serializeShip();
            view.logger.warn("Ship from file: ");
            deserializeShip();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void serializeShip() throws IOException {
        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("ship.dat"));
        out.writeObject(ship);
        out.close();
    }

    private void deserializeShip() throws IOException, ClassNotFoundException {
        ObjectInputStream in= new ObjectInputStream(new FileInputStream("ship.dat"));
        shipFromFile = (Ship)in.readObject();
        in.close();
        view.logger.info(shipFromFile);
    }
}
