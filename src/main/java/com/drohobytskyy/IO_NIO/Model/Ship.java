package com.drohobytskyy.IO_NIO.Model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class Ship implements Serializable {
    private String name;
    private ShipType type;
    List<Droid> droids = new LinkedList<Droid>();

    public Ship() {
    }

    public Ship(String name, ShipType type, List<Droid> droids) {
        this.name = name;
        this.type = type;
        this.droids = droids;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ShipType getType() {
        return type;
    }

    public void setType(ShipType type) {
        this.type = type;
    }

    public List<Droid> getDroids() {
        return droids;
    }

    public void setDroids(List<Droid> droids) {
        this.droids = droids;
    }

    @Override
    public String toString() {
        return "Ship{" +
                "name='" + name + '\'' +
                ", type=" + type + "\n" +
                ", droids=" + droids +
                '}';
    }
}
