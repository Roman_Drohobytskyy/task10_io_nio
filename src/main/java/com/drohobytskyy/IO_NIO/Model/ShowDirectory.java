package com.drohobytskyy.IO_NIO.Model;

import com.drohobytskyy.IO_NIO.View.ConsoleView;

import java.io.File;
import java.util.Arrays;

public class ShowDirectory {
    public ConsoleView view;

    public void showSpecificDirectory() {
        File directory = new File("J:\\Java");
        File[] contentsOfDirectory = directory.listFiles();
        view.logger.warn("DIRECTORIES: ");
        Arrays.stream(contentsOfDirectory)
                .filter(obj -> obj.isDirectory())
                .sorted()
                .forEach((obj) -> view.logger.info("Directory: " + obj.getName()));
        view.logger.warn("FILES: ");
        Arrays.stream(contentsOfDirectory)
                .filter(obj -> obj.isFile())
                .sorted()
                .forEach((obj) -> view.logger.info("File     : " + obj.getName() + " -> bytes:" + obj.length()));
//        for (File obj: contentsOfDirectory) {
//            if (obj.isDirectory()) {
//                System.out.println("Directory: " + obj.getName());
//            } else {
//                System.out.println("File     : " + obj.getName());
//            }
//        }
    }

}
