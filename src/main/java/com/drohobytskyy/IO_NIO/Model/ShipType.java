package com.drohobytskyy.IO_NIO.Model;

public enum ShipType {
    WARSHIP, TRANSPORT, FERRY
}
