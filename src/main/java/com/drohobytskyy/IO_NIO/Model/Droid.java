package com.drohobytskyy.IO_NIO.Model;

import java.io.Serializable;

public class Droid implements Serializable {
    private String name;
    private int power;
    private int batteryCapacity;
    private transient String keyWordForSelfDestruction;

    public Droid() {
    }

    public Droid(String name, int power, int batteryCapacity, String keyWordForSelfDestruction) {
        this.name = name;
        this.power = power;
        this.batteryCapacity = batteryCapacity;
        this.keyWordForSelfDestruction = keyWordForSelfDestruction;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public int getBatteryCapacity() {
        return batteryCapacity;
    }

    public void setBatteryCapacity(int batteryCapacity) {
        this.batteryCapacity = batteryCapacity;
    }

    @Override
    public String toString() {
        return "Droid{" +
                "name='" + name + '\'' +
                ", power=" + power +
                ", batteryCapacity=" + batteryCapacity +
                ", keyWordForSelfDestruction='" + keyWordForSelfDestruction + '\'' +
                '}';
    }
}
