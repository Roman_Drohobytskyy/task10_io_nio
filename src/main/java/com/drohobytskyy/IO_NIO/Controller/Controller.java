package com.drohobytskyy.IO_NIO.Controller;

import com.drohobytskyy.IO_NIO.Model.SerializationOfShipWithDroids;
import com.drohobytskyy.IO_NIO.Model.ShowDirectory;
import com.drohobytskyy.IO_NIO.View.ConsoleView;

import java.io.IOException;

public class Controller {
    private final static String DIVIDER = "----------------------------------------------------------------------" +
            "-----------------------------------------------------------------";
    private ConsoleView view;
    private SerializationOfShipWithDroids serializationOfShipWithDroids;
    private ShowDirectory showDirectory;

    public Controller(ConsoleView view) throws IOException {
        this.view = view;
        view.controller = this;
        serializationOfShipWithDroids = new SerializationOfShipWithDroids();
        serializationOfShipWithDroids.view = view;
        showDirectory = new ShowDirectory();
        showDirectory.view = view;
        view.createMenu();
        view.executeMenu(view.menu);
    }

    public void serializeAndDeserializeShipWithDroids() {
        view.logger.warn(DIVIDER);
        serializationOfShipWithDroids.serializeAndDeserializeShipWithDroids();
    }

    public void showSpecificDirectory() {
        view.logger.warn(DIVIDER);
        showDirectory.showSpecificDirectory();
    }
}
