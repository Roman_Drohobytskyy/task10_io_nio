package com.drohobytskyy.IO_NIO;

import com.drohobytskyy.IO_NIO.Controller.Controller;
import com.drohobytskyy.IO_NIO.View.ConsoleView;

import java.io.IOException;

public class StartPoint {

    public static void main(String[] args) {
        {
            try {
                Controller controller = new Controller(new ConsoleView());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
