package com.drohobytskyy.IO_NIO.View;

import java.io.IOException;

@FunctionalInterface
public interface Printable {

    void print() throws IOException;
}

